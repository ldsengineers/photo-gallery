import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

// import toast ui editor
import 'codemirror/lib/codemirror.css';
import 'tui-editor/dist/tui-editor.min.css';
import 'tui-editor/dist/tui-editor-contents.min.css';
// import { Editor } from '@toast-ui/react-editor';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
